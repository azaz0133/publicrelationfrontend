import React from 'react'
import Modal from 'react-responsive-modal'
import {
    compose,
    withState,
    withHandlers
} from 'recompose'
import ReactModalLogin from 'react-modal-login'

const Header = ({
 children,onOpenModal,onCloseModal,isOpen
}) => (
        <div>
            <ReactModalLogin
                visible={isOpen}
                onCloseModal={onCloseModal}
            />
            <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <span>
                    <strong>
                        ระบบสารสนเทศ การประชาสัมพันธ์ <br />คณะวิทยาศาสตร์
                    </strong>
                </span>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                <a class="navbar-item">
                    Home
                </a>

                <a class="navbar-item">
                    Documentation
                </a>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                     ประชาสัมพันธ์
                    </a>

                    <div class="navbar-dropdown">
                    <a class="navbar-item">
                        Kiosk
                    </a>
                    <a class="navbar-item">
                        เสียงตามสาย
                    </a>
                    <a class="navbar-item">
                        โทรทัศน์วงจรปิด
                    </a>
                    <hr class="navbar-divider" />
                    <a class="navbar-item">
                        Report an issue
                    </a>
                    </div>
                </div>
                </div>

                <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                    <a class="button is-primary">
                        <strong>Sign up</strong>
                    </a>
                    <a class="button is-light" onClick={onOpenModal}>
                        Log in
                    </a>
                    </div>
                </div>
                </div>
            </div>
        </nav>
        {children}
        </div>
        
)

export default compose(
 withState('isOpen','setModal',false),
 withHandlers({
     onOpenModal:({setModal,isOpen})=>_=>{
        setModal(true)
     },
     onCloseModal:({setModal,isOpen})=>_=>{
         setModal(false)
     }
 })
)(Header)
