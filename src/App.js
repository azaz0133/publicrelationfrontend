import React, { Component } from 'react';
import {Route,Switch} from 'react-router-dom'
import Header from './components/header'
import Kiosk from './containers/kiosk'
const App = ({

}) => (
    <div>
        <Header>
       <Switch>
          <Route exact path='/' component={()=>(<h1>TEST</h1>)}/>
          <Route path='/kiosk' component={Kiosk} />
       </Switch>
        </Header>
    </div>
)

export default App