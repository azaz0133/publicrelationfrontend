import React from 'react'
import {
 Form,
 Label,
 Input,
 Col,
 Row,
 FormGroup
} from 'reactstrap'
import ImageUploader from 'react-images-upload';

const Kiosk = ({

}) => (
    <div className="container">
         <Form>
             <FormGroup>
                 <Row>
                    <Label sm={2}>ชื่อสื่อดิจิตอล</Label>
                    <Col sm={4}>
                    <Input type="text" name="digital" />
                    </Col>
                 </Row>
                 <Row>
                    <Label sm={2}>อับโหลดไฟล์</Label>
                    <Col sm={4}>
                    <ImageUploader
                        withIcon={true}
                        buttonText='Choose images'
                        onChange={()=>console.log("sss")}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                    />
                    </Col>
                 </Row>
             </FormGroup>
         </Form>
    </div>
)

export default Kiosk